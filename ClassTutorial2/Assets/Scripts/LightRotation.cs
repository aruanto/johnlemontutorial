﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightRotation : MonoBehaviour
{
    public GameObject dirLight;
    public Transform lightTransform;
    public float currentValue;

    private void Start()
    {
        lightTransform = dirLight.GetComponent<Transform>();
    }

    private void FixedUpdate()
    {
        lightTransform.RotateAround(new Vector3(0,0,0), Vector3.up, 5f * Time.deltaTime );
    }

}
