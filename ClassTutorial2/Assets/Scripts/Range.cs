﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class Range : MonoBehaviour
{
    public PostProcessVolume postProcess;
    public Vignette vignette;
    public Color color1;
    public Color color2;

    private void Start()
    {
        postProcess.profile.TryGetSettings(out vignette);
    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Enemy"))
        {
            vignette.intensity.Override(0.8f);
            vignette.color.Override(color1);
            Debug.Log("inRange");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            vignette.intensity.Override(0.6f);
            vignette.color.Override(color2);
            Debug.Log("ExitingRange");
        }
    }
}
